msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-06-11 00:15+0000\n"
"PO-Revision-Date: 2023-09-16 10:09\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/okular/okular_mobi.pot\n"
"X-Crowdin-File-ID: 5795\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Guo Yunhe, Ni Hui, Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "i@guoyunhe.me, shuizhuyuanluo@126.com, tysontan@tysontan.com"

#: converter.cpp:59
#, kde-format
msgid "Error while opening the Mobipocket document."
msgstr "打开 Mobipocket 文档时出错。"

#: converter.cpp:64
#, kde-format
msgid ""
"This book is protected by DRM and can be displayed only on designated device"
msgstr "此电子书使用 DRM 保护，只能在指定的设备上显示"

#: generator_mobi.cpp:25
#, kde-format
msgid "Mobipocket"
msgstr "Mobipocket"

#: generator_mobi.cpp:25
#, kde-format
msgid "Mobipocket Backend Configuration"
msgstr "Mobipocket 后端程序配置"
