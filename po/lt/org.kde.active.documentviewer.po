# Lithuanian translations for l package.
# Copyright (C) 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
#
# Automatically generated, 2012.
# Liudas <liudas.alisauskas@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-11 00:43+0000\n"
"PO-Revision-Date: 2022-11-16 23:21+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.2.1\n"

#: package/contents/ui/Bookmarks.qml:20 package/contents/ui/OkularDrawer.qml:85
msgid "Bookmarks"
msgstr "Žymelės"

#: package/contents/ui/CertificateViewerDialog.qml:21
msgid "Certificate Viewer"
msgstr "Liudijimų žiūryklė"

#: package/contents/ui/CertificateViewerDialog.qml:32
msgid "Issued By"
msgstr "Išdavė"

#: package/contents/ui/CertificateViewerDialog.qml:37
#: package/contents/ui/CertificateViewerDialog.qml:65
msgid "Common Name:"
msgstr "Bendrasis pavadinimas:"

#: package/contents/ui/CertificateViewerDialog.qml:43
#: package/contents/ui/CertificateViewerDialog.qml:71
msgid "EMail:"
msgstr "El. paštas:"

#: package/contents/ui/CertificateViewerDialog.qml:49
#: package/contents/ui/CertificateViewerDialog.qml:77
msgid "Organization:"
msgstr "Organizacija:"

#: package/contents/ui/CertificateViewerDialog.qml:60
msgid "Issued To"
msgstr "Kam išduotas"

#: package/contents/ui/CertificateViewerDialog.qml:88
msgid "Validity"
msgstr "Tikrumas"

#: package/contents/ui/CertificateViewerDialog.qml:93
msgid "Issued On:"
msgstr "Išduotas:"

#: package/contents/ui/CertificateViewerDialog.qml:99
msgid "Expires On:"
msgstr "Galioja iki:"

#: package/contents/ui/CertificateViewerDialog.qml:110
msgid "Fingerprints"
msgstr "Kontroliniai kodai"

#: package/contents/ui/CertificateViewerDialog.qml:115
msgid "SHA-1 Fingerprint:"
msgstr "SHA-1 kontrolinis kodas:"

#: package/contents/ui/CertificateViewerDialog.qml:121
msgid "SHA-256 Fingerprint:"
msgstr "SHA-256 kontrolinis kodas:"

#: package/contents/ui/CertificateViewerDialog.qml:135
msgid "Export..."
msgstr "Eksportuoti..."

#: package/contents/ui/CertificateViewerDialog.qml:141
#: package/contents/ui/SignaturePropertiesDialog.qml:148
msgid "Close"
msgstr "Užverti"

#: package/contents/ui/CertificateViewerDialog.qml:149
msgid "Certificate File (*.cer)"
msgstr "Liudijimo failas (*.cer)"

#: package/contents/ui/CertificateViewerDialog.qml:164
#: package/contents/ui/SignaturePropertiesDialog.qml:168
msgid "Error"
msgstr "Klaida"

#: package/contents/ui/CertificateViewerDialog.qml:166
msgid "Could not export the certificate."
msgstr "Nepavyko eksportuoti liudijimo."

#: package/contents/ui/main.qml:24 package/contents/ui/main.qml:65
msgid "Okular"
msgstr "Okular"

#: package/contents/ui/main.qml:41
msgid "Open..."
msgstr "Atverti..."

#: package/contents/ui/main.qml:48
msgid "About"
msgstr "Apie"

#: package/contents/ui/main.qml:105
msgid "Password Needed"
msgstr "Reikia slaptažodžio"

#: package/contents/ui/MainView.qml:25
msgid "Remove bookmark"
msgstr "Šalinti žymelę"

#: package/contents/ui/MainView.qml:25
msgid "Bookmark this page"
msgstr "Įtraukti šį puslapį į žymeles"

#: package/contents/ui/MainView.qml:82
msgid "No document open"
msgstr "Neatvertas joks dokumentas"

#: package/contents/ui/OkularDrawer.qml:57
msgid "Thumbnails"
msgstr "Miniatiūros"

#: package/contents/ui/OkularDrawer.qml:71
msgid "Table of contents"
msgstr "Turinys"

#: package/contents/ui/OkularDrawer.qml:99
msgid "Signatures"
msgstr "Parašai"

#: package/contents/ui/SignaturePropertiesDialog.qml:30
msgid "Signature Properties"
msgstr "Parašo savybės"

#: package/contents/ui/SignaturePropertiesDialog.qml:44
msgid "Validity Status"
msgstr "Tikrumo būsena"

#: package/contents/ui/SignaturePropertiesDialog.qml:50
msgid "Signature Validity:"
msgstr "Parašo tikrumas:"

#: package/contents/ui/SignaturePropertiesDialog.qml:56
msgid "Document Modifications:"
msgstr "Dokumento modifikacijų:"

#: package/contents/ui/SignaturePropertiesDialog.qml:63
msgid "Additional Information"
msgstr "Papildoma informacija"

#: package/contents/ui/SignaturePropertiesDialog.qml:72
msgid "Signed By:"
msgstr "Pasirašė:"

#: package/contents/ui/SignaturePropertiesDialog.qml:78
msgid "Signing Time:"
msgstr "Pasirašymo laikas:"

#: package/contents/ui/SignaturePropertiesDialog.qml:84
msgid "Reason:"
msgstr "Priežastis:"

#: package/contents/ui/SignaturePropertiesDialog.qml:91
msgid "Location:"
msgstr "Vieta:"

#: package/contents/ui/SignaturePropertiesDialog.qml:100
msgid "Document Version"
msgstr "Dokumento versija"

#: package/contents/ui/SignaturePropertiesDialog.qml:110
msgctxt "Document Revision <current> of <total>"
msgid "Document Revision %1 of %2"
msgstr "Dokumento revizija %1 iš %2"

#: package/contents/ui/SignaturePropertiesDialog.qml:114
msgid "Save Signed Version..."
msgstr "Įrašyti pasirašytą versiją..."

#: package/contents/ui/SignaturePropertiesDialog.qml:128
msgid "View Certificate..."
msgstr "Rodyti liudijimą..."

#: package/contents/ui/SignaturePropertiesDialog.qml:170
msgid "Could not save the signature."
msgstr "Nepavyko įrašyti parašo."

#: package/contents/ui/Signatures.qml:27
msgid "Not Available"
msgstr "Neprieinama"

#: package/contents/ui/ThumbnailsBase.qml:43
msgid "No results found."
msgstr "Rezultatų nerasta."

#~ msgid "Search..."
#~ msgstr "Ieškoti..."

#~ msgid "Document viewer for Plasma Active using Okular"
#~ msgstr "Dokumentų žiūryklė skirta Plasma Active naudojant Okular"

#~ msgid "Reader"
#~ msgstr "Skaitytuvas"

#~ msgid "Copyright 2012 Marco Martin"
#~ msgstr "Autorinės teisės 2012 Marco Martin"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "URL of the file to open"
#~ msgstr "Atidaromo failo URL"
